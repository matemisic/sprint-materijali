
var t = 350;
var fntSize = 48;
var words = [];

var initTime;
var mintime;
var stepSize; 
var shouldContinue = false;
var wordIndex = 0;

async function start(){

	await display();

}

async function display() {

	words = $('#inpt').val().replace(/[\r\n]+|\s{2,}/g, ' ').split(' ');

	initTime = parseInt(document.getElementById("init-time").value);
	mintime = parseInt(document.getElementById("min-time").value);
	stepSize = parseInt(document.getElementById("step-size").value);
	t = initTime;
	shouldContinue = true;
	for (; wordIndex < words.length; wordIndex++){


		if(shouldContinue === false){
			stop();
			return;
		}

		await sleep(t);
		if (t > mintime){
			t-=stepSize;
		}
		if (t < mintime){
			t = mintime;
		}
		//content.innerText=words[i] + " - " + t + " - " + wordIndex;
		//content.innerText=words[i];
		time_show(t)
		word_show(wordIndex, words);

	}
	stop();
}

function word_show(i, words) {
	var $space = $('.spritz-word');
	var word = words[i];
	if (word === '') {
		return;
	}
	var stop = Math.round((word.length+1)*0.4)-1;
	$space.html('<div>'+word.slice(0,stop)+'</div><div>'+word[stop]+'</div><div>'+word.slice(stop+1)+'</div>');
}

function time_show(t){
	let el = document.getElementById("current-wt");
	el.innerText = t;
}

async function rewind(){
	console.log(wordIndex);
	wordIndex-=5;
	console.log(wordIndex);
}

async function fastForward(){
	wordIndex+=5;
}

function stop() {
	wordIndex = 0;
	pause();
}

function pause() {
	shouldContinue = false;
}

function updateFont(){
	document.getElementById("content").style.fontSize = fntSize+"px";
}

function incr(){
	fntSize+=2;
	updateFont();		
}

function decr(){
	fntSize-=2;
	updateFont();
}


function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

$(function () {
	$("#resource-select").select2({
		placeholder: 'Odaberi materijal za pregled',
		width: "20em"
	});
});

$('#changeThemeBtn').click(function (){

	const theme = document.querySelector("#theme-css");
	if (theme.getAttribute("href").includes("dark")){
		theme.href=theme.getAttribute("href").replace("dark", "light")
	}else {
		theme.href=theme.getAttribute("href").replace("light", "dark")
	}

})

$('#changeFontBtn').click(function (){

	const theme = document.querySelector("#font-css");
	if (theme.getAttribute("href").includes("regular")){
		theme.href=theme.getAttribute("href").replace("regular", "dyslexic")
	}else {
		theme.href=theme.getAttribute("href").replace("dyslexic", "regular")
	}

})
