package hr.tvz.ntpws.sprintmaterijali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "User not logged in")
public class UnauthorizedException extends RuntimeException {
}
