package hr.tvz.ntpws.sprintmaterijali.controller.view;

import hr.tvz.ntpws.sprintmaterijali.model.User;
import hr.tvz.ntpws.sprintmaterijali.model.dto.UserDto;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class CredentialViewController {

    private final UserService userService;


    @GetMapping("/register/new")
    public String showRegister(Model model){
        model.addAttribute("user", new UserDto());
        return "registration";
    }

    @PostMapping("/register/new")
    public String registerSuccess(UserDto user, Model model){

        userService.register(user);
        return "redirect:/login";
    }
}
