package hr.tvz.ntpws.sprintmaterijali.controller.view;

import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class ProfileViewController {

  private final GroupService groupService;
  private final UserService userService;

  @GetMapping("/profile")
  public String profile(Model model){
    model.addAttribute("userGroups",groupService.getAllowedGroups());
    model.addAttribute("user",userService.getCurrentUser());
    return "profilePage";
  }
}
