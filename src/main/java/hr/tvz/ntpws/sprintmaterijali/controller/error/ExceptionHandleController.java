package hr.tvz.ntpws.sprintmaterijali.controller.error;

import hr.tvz.ntpws.sprintmaterijali.exception.UnauthorizedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandleController {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public String handleUnauthorised(Model model){
        model.addAttribute("message", HttpStatus.UNAUTHORIZED);
        return "error.html";
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public String handleDAOException(Model model){
        model.addAttribute("message", HttpStatus.BAD_REQUEST);
        return "error.html";
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleRuntimeException(Model model, Exception e){
        model.addAttribute("message", e.getMessage());
        return "error.html";
    }

}
