package hr.tvz.ntpws.sprintmaterijali.controller.rest;

import hr.tvz.ntpws.sprintmaterijali.model.Message;
import hr.tvz.ntpws.sprintmaterijali.service.MessagingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/message")
public class MessageController {

    private final MessagingService messagingService;

    @GetMapping
    public List<Message> getMessages(Integer groupId, Integer page) {
        return messagingService.getMessages(groupId, page);
    }

    @PostMapping
    public void sendMessage(String content, Integer groupId) {
        messagingService.sendMessage(content, groupId);
    }

}
