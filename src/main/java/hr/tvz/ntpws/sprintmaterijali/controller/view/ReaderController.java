package hr.tvz.ntpws.sprintmaterijali.controller.view;

import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import hr.tvz.ntpws.sprintmaterijali.service.ResourceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.NoSuchElementException;

@Controller
@RequestMapping("/reader")
@RequiredArgsConstructor
@Slf4j
public class ReaderController {

    private final ResourceService resourceService;

    @GetMapping
    public String reader() {
        return "reader";
    }

    @GetMapping("/{id}")
    public String readerParam(@PathVariable Integer id, Model model) {

        try {
            Resource resource = resourceService.getResource(id);
            model.addAttribute("resourceContent", resource.getContent());
            model.addAttribute("resourceTitle", resource.getTitle());
        }catch (NoSuchElementException e){
            log.debug("Tried to open resource {} which does not exist", id);
        }
        return "reader";
    }

}
