package hr.tvz.ntpws.sprintmaterijali.controller.rest;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.GroupInvitations;
import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/group")
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @GetMapping
    public List<Group> getMyCurrentGroups() {
        List<Group> allowedGroups = groupService.getAllowedGroups();
        return allowedGroups;
    }

    @PostMapping
    public Group creatGroup(String groupName, Boolean isPublic) {
        Group group = groupService.create(groupName, isPublic);
        return group;
    }

    @GetMapping("/invitations")
    public List<GroupInvitations> get(){
        return groupService.getUserInvitations();
    }

    @GetMapping("/joinPublic/{id}")
    public Group joinPublic(@PathVariable Integer id){
        return groupService.joinPublic(id);
    }

}
