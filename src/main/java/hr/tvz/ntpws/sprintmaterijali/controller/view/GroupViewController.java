package hr.tvz.ntpws.sprintmaterijali.controller.view;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.Message;
import hr.tvz.ntpws.sprintmaterijali.model.dto.FormGroupDto;
import hr.tvz.ntpws.sprintmaterijali.model.dto.FormMessageDto;
import hr.tvz.ntpws.sprintmaterijali.model.dto.FormNewInvitationDto;
import hr.tvz.ntpws.sprintmaterijali.model.dto.FormResourceDto;
import hr.tvz.ntpws.sprintmaterijali.model.dto.GroupInvitationDto;
import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.MessagingService;
import hr.tvz.ntpws.sprintmaterijali.service.ResourceService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Comparator;

@Controller
@RequiredArgsConstructor
public class GroupViewController {

  private final GroupService groupService;
  private final MessagingService messagingService;
  private final UserService userService;
  private final ResourceService resourceService;

  @GetMapping("/newGroup")
  public String creteGroupForm(Model model){
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    model.addAttribute("groupDto", new FormGroupDto());
    return "groupNewPage";
  }
  @PostMapping("/createGroup")
  public String createGroup(@ModelAttribute("groupDto") @Valid FormGroupDto groupDto, Errors errors, Model model){
    if(errors.hasErrors()){
      model.addAttribute("userGroups", groupService.getAllowedGroups());
      model.addAttribute("groupDto", groupDto);
      return "groupNewPage";
    }
    groupService.create(groupDto.getName(),groupDto.privacyType);
    return "redirect:/";
  }

  @GetMapping("/group/{id}")
  public String groupDetails(@PathVariable("id") Integer groupId, Model model){
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    Group group = groupService.getIfAuthorised(groupId).get();
    model.addAttribute("group", group);
    model.addAttribute("groupMessages",messagingService.getMessages(groupId,1));
    model.addAttribute("newMessage", new FormMessageDto());
    return "groupPage";
  }

  @PostMapping("/group/{id}/newMessage")
  public String createMessage(@PathVariable("id") Integer groupId ,@Valid @ModelAttribute("newMessage") FormMessageDto messageDto, Errors errors,Model model){
    if(errors.hasErrors()){
      return "redirect:/group/" + groupId;
    }
    messagingService.sendMessage(messageDto.getMessageContent(), groupId);
    return "redirect:/group/" + groupId;
  }

  @GetMapping("/group/{id}/newUser")
  public String newUser(@PathVariable("id") Integer groupId,Model model){
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
    model.addAttribute("invitation", new FormNewInvitationDto());
    return "groupNewUser";
  }

  @PostMapping("/group/{id}/inviteUser")
  public String inviteUserInGroup(@PathVariable("id") Integer groupId,@ModelAttribute("invitation") @Valid FormNewInvitationDto invitationDto,Errors errors, Model model){
    if(errors.hasErrors()){
      model.addAttribute("userGroups", groupService.getAllowedGroups());
      model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
      model.addAttribute("invitation", invitationDto);
      return "groupNewUser";
    }
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
    model.addAttribute("invitation", new GroupInvitationDto());
    try {
      groupService.invite(invitationDto.getInvitedUsername(),invitationDto.getInvitationMessage(),groupId);
    }catch (RuntimeException ex){
      model.addAttribute("invitedUserError", true);
      model.addAttribute("userGroups", groupService.getAllowedGroups());
      model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
      model.addAttribute("invitation", invitationDto);
      return "groupNewUser";
    }
    return "redirect:/group/" + groupId;
  }
  @GetMapping("/group/{id}/removeUser/{userId}")
  public String removeUserFromGroup(@PathVariable("id") Integer groupId,@PathVariable("userId")Integer removeUserId,Model model){
    groupService.removeUser(removeUserId,groupId);
    return "redirect:/group/" + groupId;
  }

  @GetMapping("/group/{id}/newResource")
  public String newResource(@PathVariable("id") Integer groupId,Model model){
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
    model.addAttribute("resourceDto", new FormResourceDto());
    return "groupNewResourcePage";
  }
  @PostMapping("/group/{id}/createResource")
  public String creteResource(@PathVariable("id") Integer groupId,@ModelAttribute("resourceDto") @Valid FormResourceDto resourceDto,Errors errors, Model model){
    if(errors.hasErrors()){
      model.addAttribute("userGroups", groupService.getAllowedGroups());
      model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
      model.addAttribute("resourceDto", resourceDto);
      return "groupNewResourcePage";
    }
    resourceService.sendResource(resourceDto.getTitle(),resourceDto.getContent(),groupId);
    model.addAttribute("userGroups", groupService.getAllowedGroups());
    model.addAttribute("group", groupService.getIfAuthorised(groupId).get());
    return "redirect:/group/" + groupId;
  }

  @GetMapping("/joinPublic/{id}")
  public String joinPublicGroup(@PathVariable("id") Integer groupId){
    groupService.joinPublic(groupId);
    return "redirect:/";
  }
}
