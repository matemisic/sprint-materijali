package hr.tvz.ntpws.sprintmaterijali.controller.view;

import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.JokeService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ViewController {

    private final GroupService groupService;
    private final JokeService jokeService;
    private final UserService userService;

    @GetMapping("/")
    public String showFistPage(Model model) {
        model.addAttribute("userGroups", groupService.getAllowedGroups());
        model.addAttribute("notJoinedPublicGroups", groupService.getNotJoinedPublicGroups());
        model.addAttribute("userInvitations",groupService.getUserInvitations());
        model.addAttribute("joke", jokeService.getJoke());

        return "firstPage";
    }

    @GetMapping("/acceptInvite/{id}")
    public String acceptInvite(@PathVariable("id") Integer invitationId,Model model){
        model.addAttribute("userGroups", groupService.getAllowedGroups());
        model.addAttribute("userInvitations",groupService.getUserInvitations());
        groupService.acceptInvitation(invitationId);
        return "redirect:/";
    }

    @GetMapping("/declineInvite/{id}")
    public String declineInvite(@PathVariable("id") Integer invitationId,Model model){
        model.addAttribute("userGroups", groupService.getAllowedGroups());
        model.addAttribute("userInvitations",groupService.getUserInvitations());
        groupService.declineInvitation(invitationId);
        return "redirect:/";
    }

    @PostMapping("/promote")
    public String promoteMe(Model model){
        userService.promoteToEditor(userService.getCurrentUser().getUsername());
        return "redirect:/";
    }

    @GetMapping("/about")
    public String about(Model model){
        model.addAttribute("userGroups", groupService.getAllowedGroups());
        return "about";
    }
}
