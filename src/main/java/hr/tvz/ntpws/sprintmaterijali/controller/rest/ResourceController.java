package hr.tvz.ntpws.sprintmaterijali.controller.rest;

import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import hr.tvz.ntpws.sprintmaterijali.service.ResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/resource")
public class ResourceController {

    private final ResourceService resourceService;

    @GetMapping
    public Page<Resource> getMessages(Integer groupId, Integer page) {
        return resourceService.getResources(groupId, page);
    }

    @PostMapping
    public void sendMessage(String title, String content, Integer groupId) {
        resourceService.sendResource(title, content, groupId);
    }

    @PostMapping("/add")
    public void addResourceToGroup(Integer resourceId, Integer groupId){
        resourceService.addToGroup(resourceId, groupId);
    }

    @GetMapping("/{id}")
    public Resource getResource(@PathVariable Integer id){
        return resourceService.getResource(id);
    }
}
