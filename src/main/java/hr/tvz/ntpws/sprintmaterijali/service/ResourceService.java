package hr.tvz.ntpws.sprintmaterijali.service;

import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import org.springframework.data.domain.Page;

public interface ResourceService {

    void sendResource(String title, String messageContent, Integer groupId);

    Page<Resource> getResources(Integer groupId, Integer page);

    void addToGroup(Integer resourceId, Integer newAllowedGroupId);

    Resource getResource(Integer id);


}
