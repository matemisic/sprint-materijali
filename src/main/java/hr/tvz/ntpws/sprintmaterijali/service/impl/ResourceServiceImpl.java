package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import hr.tvz.ntpws.sprintmaterijali.repository.ResourceRepository;
import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.ResourceService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ResourceServiceImpl implements ResourceService {

    private final GroupService groupService;
    private final ResourceRepository resourceRepository;
    private final UserService userService;

    @Override
    public void sendResource(String title, String messageContent, Integer groupId) {

        Resource r = new Resource();
        r.setContent(messageContent);
        r.setTitle(title);
        r.setCreatedAt(LocalDateTime.now());
        r.setUser(userService.getCurrentUser());

        Group g = groupService.getIfAuthorised(groupId).orElseThrow();
        r.setGroup(g);
        r.getAllowed().add(g);
        resourceRepository.save(r);

    }

    @Override
    public Page<Resource> getResources(Integer groupId, Integer page) {
        return resourceRepository.findAll(PageRequest.of(page-1, 20, Sort.by("createdAt").ascending()));
    }

    @Override
    public void addToGroup(Integer resourceId, Integer newAllowedGroupId) {

        Group g = groupService.getIfAuthorised(newAllowedGroupId).orElseThrow();
        Resource r = resourceRepository.getOne(resourceId);
        r.getAllowed().add(g);
        resourceRepository.save(r);
    }

    @Override
    public Resource getResource(Integer id) {
        // This should be secured
        return resourceRepository.findById(id).orElseThrow();
    }
}
