package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.GroupInvitations;
import hr.tvz.ntpws.sprintmaterijali.model.User;
import hr.tvz.ntpws.sprintmaterijali.model.dto.GroupInvitationDto;
import hr.tvz.ntpws.sprintmaterijali.repository.GroupInvitationRepository;
import hr.tvz.ntpws.sprintmaterijali.repository.GroupRepository;
import hr.tvz.ntpws.sprintmaterijali.repository.UserRepository;
import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import hr.tvz.ntpws.sprintmaterijali.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final GroupInvitationRepository groupInvitationRepository;

    @Override
    public Group create(String groupName, Boolean isPublic) {
        Group group = new Group();
        group.setIsPublic(isPublic);
        group.setName(groupName);

        User currentUser = userService.getCurrentUser();
        group.setOwner(currentUser);
        group.getMembers().add(currentUser);
        currentUser.getGroups().add(group);

        group = groupRepository.save(group);
        userRepository.save(currentUser);
        return group;
    }

    @Override
    public void delete(Integer groupId) {
        groupRepository.deleteById(groupId);
    }

    @Override
    public void removeUser(Integer userId, Integer groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow();
        User removed = userRepository.findById(userId).orElseThrow();
        group.getMembers().remove(removed);
        groupRepository.save(group);
        removed.getGroups().remove(group);
        userRepository.save(removed);
    }


    @Override
    public void changeOwner(Integer newOwnerId, Integer groupId) {

        Optional<Group> group = groupRepository.findById(groupId);
        group.ifPresent((persistedGroup) -> {
            userRepository.findById(newOwnerId).ifPresent((persistedUser) -> {
                persistedGroup.setOwner(persistedUser);
                groupRepository.save(persistedGroup);
            });
        });

    }

    @Override
    public void invite(GroupInvitationDto invitation) {
        groupRepository.findById(invitation.getGroupId()).ifPresent((group -> {
            userRepository.findById(invitation.getInvitedById()).ifPresent((invitedBy) -> {
                if (group.getOwner().equals(invitedBy)) {
                    userRepository.findById(invitation.getInvitedUserId()).ifPresent((invitedUser) -> {
                        groupInvitationRepository.save(new GroupInvitations(invitedUser, invitedBy, group, invitation.getInvitationMessage()));
                    });
                }
            });
        }));


    }

    @Override
    public void invite(String username, String message, Integer groupId) {
        Group g = getIfAuthorised(groupId).orElseThrow(() -> new RuntimeException("Group does not exist or user does not have permission"));
        User u = userRepository.findByUsername(username);
        if (u == null){
            throw new RuntimeException("User does not exist");
        }
        groupInvitationRepository.save(new GroupInvitations(u, userService.getCurrentUser(), g, message));
    }

    @Override
    public Group acceptInvitation(Integer invitationId) {
        Optional<GroupInvitations> invitation = groupInvitationRepository.findById(invitationId);
        if (invitation.isPresent()) {
            if (invitation.get().getInvited().getUsername().equals(UserUtil.getCurrentUser())) {
                invitation.get().getGroup().getMembers().add(invitation.get().getInvited());
                Group persistedGroup = groupRepository.save(invitation.get().getGroup());

                invitation.get().getInvited().getGroups().add(invitation.get().getGroup());
                userRepository.save(invitation.get().getInvited());

                groupInvitationRepository.delete(invitation.get());
                return persistedGroup;
            }
        }
        throw new RuntimeException("Group or user no longer exists");
    }

    @Override
    public void declineInvitation(Integer invitationId) {
        GroupInvitations inv = groupInvitationRepository.findById(invitationId).orElseThrow(() -> new RuntimeException("Invitation does not exist"));
        User currentUser = userService.getCurrentUser();

        if (!inv.getInvited().equals(currentUser)){
            throw new RuntimeException("Cannot decline invitation for another user");
        }
        groupInvitationRepository.delete(inv);
    }

    @Override
    public Optional<Group> getIfAuthorised(Integer groupId) {
        Optional<Group> group = groupRepository.findById(groupId);
        return group.isPresent() && group.get().getMembers().contains(userService.getCurrentUser()) ? group : Optional.empty();
    }

    @Override
    public Group joinPublic(Integer groupId) {
        Optional<Group> group = groupRepository.findById(groupId);
        if (group.isPresent() && group.get().getIsPublic()) {
            group.get().getMembers().add(userService.getCurrentUser());
            userService.getCurrentUser().getGroups().add(group.get());
            userRepository.save(userService.getCurrentUser());
            groupRepository.save(group.get());
            return group.get();
        } else {
            throw new RuntimeException("Group is not public");
        }
    }

    @Override
    public List<Group> getAllowedGroups() {
        return groupRepository.findAllByMembersContaining(userService.getCurrentUser());
    }

    @Override
    public List<Group> getPublicGroups() {
        return groupRepository.findAllByIsPublic(true);
    }

    @Override
    public List<Group> getNotJoinedPublicGroups() {
        return groupRepository.findAllByIsPublicAndMembersNotContaining(true, userService.getCurrentUser());
    }

    @Override
    public List<GroupInvitations> getUserInvitations() {
        return getUserInvitations(UserUtil.getCurrentUser());
    }

    @Override
    public List<GroupInvitations> getUserInvitations(String username) {
        return groupInvitationRepository.findAllByInvited_Username(username);
    }
}
