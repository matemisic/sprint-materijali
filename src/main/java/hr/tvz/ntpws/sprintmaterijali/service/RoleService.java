package hr.tvz.ntpws.sprintmaterijali.service;

import hr.tvz.ntpws.sprintmaterijali.model.Role;
import hr.tvz.ntpws.sprintmaterijali.model.RoleType;

public interface RoleService {

    Role getOrCreate(RoleType roleType);
}
