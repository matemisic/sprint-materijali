package hr.tvz.ntpws.sprintmaterijali.service;

import hr.tvz.ntpws.sprintmaterijali.model.Message;

import java.util.List;

public interface MessagingService {

    void sendMessage(String messageContent, Integer groupId);

    List<Message> getMessages(Integer groupId, Integer page);


}
