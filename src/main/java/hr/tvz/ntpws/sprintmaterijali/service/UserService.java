package hr.tvz.ntpws.sprintmaterijali.service;

import hr.tvz.ntpws.sprintmaterijali.model.User;
import hr.tvz.ntpws.sprintmaterijali.model.dto.UserDto;

public interface UserService {

    User register(UserDto user);
    User updatePassword(String cleartextPassword);
    User findById(Integer id);
    User promoteToEditor(String username);
    User getCurrentUser();
}
