package hr.tvz.ntpws.sprintmaterijali.service;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.GroupInvitations;
import hr.tvz.ntpws.sprintmaterijali.model.dto.GroupInvitationDto;

import java.util.List;
import java.util.Optional;

public interface GroupService {

    Group create(String groupName, Boolean isPublic);

    void delete(Integer groupId);

    void removeUser(Integer userId, Integer groupId);

    void changeOwner(Integer newOwnerId, Integer groupId);

    void invite(GroupInvitationDto invitation);

    void invite(String username, String message, Integer groupId);

    Group acceptInvitation(Integer invitationId);

    void declineInvitation(Integer invitationId);

    Optional<Group> getIfAuthorised(Integer groupId);

    Group joinPublic(Integer groupId);

    List<Group> getAllowedGroups();

    List<Group> getPublicGroups();

    List<Group> getNotJoinedPublicGroups();

    List<GroupInvitations> getUserInvitations();

    List<GroupInvitations> getUserInvitations(String username);
}
