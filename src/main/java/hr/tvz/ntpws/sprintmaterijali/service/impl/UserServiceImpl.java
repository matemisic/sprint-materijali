package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.Role;
import hr.tvz.ntpws.sprintmaterijali.model.RoleType;
import hr.tvz.ntpws.sprintmaterijali.model.User;
import hr.tvz.ntpws.sprintmaterijali.model.dto.UserDto;
import hr.tvz.ntpws.sprintmaterijali.repository.RoleRepository;
import hr.tvz.ntpws.sprintmaterijali.repository.UserRepository;
import hr.tvz.ntpws.sprintmaterijali.service.RoleService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import hr.tvz.ntpws.sprintmaterijali.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final RoleRepository roleRepository;

    @Override
    public User register(UserDto user) {

        if (!Objects.equals(user.getPassword(), user.getRepeatPassword())) {
            return null;
        }

        User persisted = userRepository.findByUsername(user.getUsername());
        if (persisted != null) {
            throw new RuntimeException("User already exists");
        }
        persisted = new User();
        persisted.setUsername(user.getUsername());
        persisted.setPassword(passwordEncoder.encode(user.getPassword()));
        persisted.setEnabled(true);

        Role userRole = roleService.getOrCreate(RoleType.USER);
        persisted.getRoles().add(Objects.requireNonNullElseGet(userRole, () -> new Role(RoleType.USER)));

        persisted = userRepository.save(persisted);
        return persisted;

    }

    @Override
    public User updatePassword(String cleartextPassword) {
        String currentUser = UserUtil.getCurrentUser();
        User persisted = userRepository.findByUsername(currentUser);
        persisted.setPassword(passwordEncoder.encode(cleartextPassword));
        userRepository.save(persisted);
        return persisted;
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User promoteToEditor(String username) {
        User persisted = userRepository.findByUsername(username);
        Role editorRole = roleService.getOrCreate(RoleType.EDITOR);
        persisted.getRoles().add(editorRole);
        editorRole.getUsers().add(persisted);
        userRepository.save(persisted);
        roleRepository.save(editorRole);
        return persisted;
    }

    @Override
    public User getCurrentUser() {

        return userRepository.findByUsername(UserUtil.getCurrentUser());
    }
}
