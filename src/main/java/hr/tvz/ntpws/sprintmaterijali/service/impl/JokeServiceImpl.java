package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.dto.Joke;
import hr.tvz.ntpws.sprintmaterijali.service.JokeService;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class JokeServiceImpl implements JokeService {

    private final WebClient webClient;

    public JokeServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist&type=single").build();
    }

    @Override
    public String getJoke() {
        try {
            Joke response = webClient.get().retrieve().bodyToMono(Joke.class).block();
            if (response != null) {
                return response.getJoke();
            } else
                return "Joke service not reachable";
        }catch (Exception e){
            return "What’s the best thing about Switzerland? I don’t know, but the flag is a big plus.";
        }
    }
}
