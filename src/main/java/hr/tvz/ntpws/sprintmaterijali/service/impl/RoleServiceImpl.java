package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.Role;
import hr.tvz.ntpws.sprintmaterijali.model.RoleType;
import hr.tvz.ntpws.sprintmaterijali.repository.RoleRepository;
import hr.tvz.ntpws.sprintmaterijali.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public Role getOrCreate(RoleType roleType) {
        Role persisted = roleRepository.findByRole(roleType);
        if (persisted != null)
            return persisted;

        persisted = new Role(roleType);
        roleRepository.save(persisted);
        return persisted;
    }
}
