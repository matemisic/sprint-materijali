package hr.tvz.ntpws.sprintmaterijali.service.impl;

import hr.tvz.ntpws.sprintmaterijali.model.Message;
import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import hr.tvz.ntpws.sprintmaterijali.repository.GroupRepository;
import hr.tvz.ntpws.sprintmaterijali.repository.MessageRepository;
import hr.tvz.ntpws.sprintmaterijali.service.GroupService;
import hr.tvz.ntpws.sprintmaterijali.service.MessagingService;
import hr.tvz.ntpws.sprintmaterijali.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessagingServiceImpl implements MessagingService {

    private final UserService userService;
    private final MessageRepository messageRepository;
    private final GroupService groupService;

    @Override
    public void sendMessage(String messageContent, Integer groupId) {

        Message message = new Message();
        message.setContent(messageContent);
        message.setCreatedAt(LocalDateTime.now());
        message.setGroup(groupService.getIfAuthorised(groupId).orElseThrow(() -> new RuntimeException("User is not member of group to which message is sent")));
        message.setUser(userService.getCurrentUser());

        messageRepository.save(message);
    }

    @Override
    public List<Message> getMessages(Integer groupId, Integer page) {

        List<Message> createdAt = messageRepository.findAllByGroup_Id(groupId, PageRequest.of(page - 1, 30, Sort.by("createdAt").descending()))
                .stream()
                .filter(e ->! (e instanceof Resource))
                .collect(Collectors.toCollection(ArrayList::new));
        return createdAt;
    }
}
