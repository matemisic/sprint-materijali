package hr.tvz.ntpws.sprintmaterijali;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintMaterijaliApplication {

    public static void main(String[] args) {
        SpringApplication.run(SprintMaterijaliApplication.class, args);
    }

}
