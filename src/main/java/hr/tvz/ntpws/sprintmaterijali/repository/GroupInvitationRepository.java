package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.GroupInvitations;
import hr.tvz.ntpws.sprintmaterijali.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupInvitationRepository extends JpaRepository<GroupInvitations, Integer> {

    List<GroupInvitations> findAllByInvited(User user);
    List<GroupInvitations> findAllByInvited_Username(String username);
}
