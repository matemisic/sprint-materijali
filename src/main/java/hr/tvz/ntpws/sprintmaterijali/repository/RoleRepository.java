package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.Role;
import hr.tvz.ntpws.sprintmaterijali.model.RoleType;
import hr.tvz.ntpws.sprintmaterijali.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByRole(RoleType role);
}
