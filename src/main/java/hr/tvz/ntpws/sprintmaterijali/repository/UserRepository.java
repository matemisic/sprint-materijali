package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
}
