package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.Group;
import hr.tvz.ntpws.sprintmaterijali.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Integer> {

    List<Group> findAllByMembersContaining(User user);
    List<Group> findAllByIsPublic(Boolean isPublic);
    List<Group> findAllByIsPublicAndMembersNotContaining(Boolean isPublic, User user);
}
