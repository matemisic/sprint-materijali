package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceRepository extends JpaRepository<Resource, Integer> {
}
