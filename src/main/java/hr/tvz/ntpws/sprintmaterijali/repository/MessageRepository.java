package hr.tvz.ntpws.sprintmaterijali.repository;

import hr.tvz.ntpws.sprintmaterijali.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    Page<Message> findAllByGroup_Id(Integer groupId, Pageable page);
}
