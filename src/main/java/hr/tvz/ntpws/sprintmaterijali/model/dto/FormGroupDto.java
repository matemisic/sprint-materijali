package hr.tvz.ntpws.sprintmaterijali.model.dto;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class FormGroupDto {

  @NotNull
  @Size(min=2, max=50)
  public String name;
  public boolean privacyType;

}
