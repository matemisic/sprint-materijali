package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class FormMessageDto {

  @NotNull
  @Size(min = 1)
  public String messageContent;

}
