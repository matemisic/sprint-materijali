package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class FormNewInvitationDto {

  @NotNull
  @Size(min = 1)
  public String invitedUsername;
  public String invitationMessage;
}
