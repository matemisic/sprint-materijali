package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GroupInvitationDto {

    Integer invitedUserId;
    Integer invitedById;
    Integer groupId;
    LocalDateTime invitationTime;
    String invitationMessage;
}
