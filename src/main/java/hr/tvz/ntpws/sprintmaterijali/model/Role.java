package hr.tvz.ntpws.sprintmaterijali.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@ToString(exclude = {"users"})
@Table(name = "ROLE")
public class Role extends Persistable {

    @Enumerated(EnumType.STRING)
    private RoleType role;

    @ManyToMany(mappedBy = "roles")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private final Set<User> users = new HashSet<>();

    public Role(String role) {
        this.role = RoleType.valueOf(role);
    }

    public Role(RoleType role) {
        this.role = role;
    }

    //Needed for ORM
    protected Role() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role1 = (Role) o;

        return role == role1.role;
    }

    @Override
    public int hashCode() {
        return role.hashCode();
    }
}
