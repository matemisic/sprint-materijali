package hr.tvz.ntpws.sprintmaterijali.model;

public enum RoleType {

    USER,
    EDITOR,
    ADMIN
}
