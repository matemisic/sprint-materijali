package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class FormResourceDto {

  @NotNull
  @Size(min = 2, max = 200)
  private String title;
  @NotNull
  @Size(min = 2, max = 4000)
  private String content;
}
