package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

@Data
public class Joke {

    Boolean error;
    String category;
    String type;
    String joke;
    JokeFlags flags;
    Integer id;
    String lang;

}
