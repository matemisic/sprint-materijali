package hr.tvz.ntpws.sprintmaterijali.model.dto;

import lombok.Data;

@Data
public class JokeFlags {

    Boolean nsfw;
    Boolean religious;
    Boolean political;
    Boolean racist;
    Boolean sexist;
}
