package hr.tvz.ntpws.sprintmaterijali.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "GROUPS")
public class Group extends Persistable {

    private String name;

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne
    private User owner;

    @ManyToMany(mappedBy = "groups", cascade = CascadeType.ALL)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private List<User> members = new ArrayList<>();

    @OneToMany(mappedBy = "group")
    private List<Message> messages = new ArrayList<>();

    @ManyToMany(mappedBy = "allowed", fetch = FetchType.LAZY)
    private List<Resource> resources = new ArrayList<>();


    private Boolean isPublic;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (!Objects.equals(name, group.name)) return false;
        if (!Objects.equals(getId(), group.getId())) return false;
        return Objects.equals(isPublic, group.isPublic);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", isPublic=" + isPublic +
                '}';
    }
}
