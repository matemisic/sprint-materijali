# Sprint-materijali

Spring boot projekt za NTPWS

## Opis u dokumentaciji

Aplikacija „Sprint materijali“ bi koristila tehnologiju baziranu na rapidnom slijednom vizualnom prikazu (eng. Rapid serial visual presentation) skraćeno RSVP kako bi omogućila čitanje natprosječnom brzinom. Podjela korisnika je na studente, profesore i administratore. Materijali se mogu dodati u tekstualnom obliku ili u pdf dokumentu koji se pokuša pretvoriti u tekst. Studenti mogu kreirati materijale privatno, dodati ih u privatnu grupu ili u jednu od javnih grupa za koju imaju dopuštenje. Profesori uz sve kao i studenti mogu kreirati nove javne grupe i postavljati materijale u bilo koju grupu kojoj imaju pristup. Registrirani računi su studenti, a samo administrator može promovirati račun u profesora ili administratora. 
Platforma bi služila kao forum za dijeljenje materijala prilagođenog čitanju RSVP metodom kako bi se povećala učinkovitost vremena koje student provede aktivno učeći omogućavajući brže čitanje.
